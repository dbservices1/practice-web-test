package pt.dbservices.web.test.AddToCart

import org.openqa.selenium.WebDriver
import pt.dbservices.web.Helper.PropertyHelper
import pt.dbservices.web.PageObject.AddedToCartPopUp
import pt.dbservices.web.PageObject.HomePage
import pt.dbservices.web.PageObject.SearchListPage
import pt.dbservices.web.PageObject.OrderPage
import pt.dbservices.web.SetupDriver.DriverSetup
import spock.lang.Shared
import spock.lang.Specification

class AddToCartFromSearchListSpec extends Specification {

    @Shared
    WebDriver driver

    def setupSpec() {
        DriverSetup.setSystemPropertyDriver()
    }

    def setup(){
        driver = DriverSetup.getDriver()
    }

    def cleanup() {
        if (driver != null)
            driver.quit()
    }

    def "Add item to cart from search list"() {
        given: "i'm in my store"
        def urlMyStore = new PropertyHelper().getWebSiteUrl("myStore")
        driver.get(urlMyStore)

        and: "i have a list of itens to add to cart"
        HomePage homePage = new HomePage(driver)
        SearchListPage searchListPage = homePage.searchItem("Dress")

        when: "i add a item to cart"
        def itemToAdd = 2
        def itemName = searchListPage.getNameItemToAddInCart(itemToAdd)
        AddedToCartPopUp addedToCartPopUp = searchListPage.addItemToCart(itemToAdd)

        then: "a message of sucess on a pop appears"
        assert addedToCartPopUp.getsuccessMessage() == "Product successfully added to your shopping cart"

        and:"the item is in the cart"
        OrderPage orderPage = addedToCartPopUp.proceedToCheckOut()
        def qtdInCart = orderPage.getCartQuantity()
        assert qtdInCart == "1"

        and:"also the item is order"
        def listProducts = orderPage.getProductsNames()
        assert listProducts.contains(itemName)
    }

    def "Add multiple itens to cart from search list"() {
        given: "i'm in my store"
        def urlMyStore = new PropertyHelper().getWebSiteUrl("myStore")
        driver.get(urlMyStore)

        and: "i have a item added in cart"
        HomePage homePage = new HomePage(driver)
        SearchListPage searchListPage = homePage.searchItem("Dress")

        def itemToAdd = 2
        def itensNames = []
        itensNames.add(searchListPage.getNameItemToAddInCart(itemToAdd))
        AddedToCartPopUp addedToCartPopUp = searchListPage.addItemToCart(itemToAdd)
        searchListPage = addedToCartPopUp.continueShopping()

        when: "i add a other item to cart"
        def otherItemToAdd = 3
        itensNames.add(searchListPage.getNameItemToAddInCart(otherItemToAdd))
        addedToCartPopUp = searchListPage.addItemToCart(otherItemToAdd)

        then: "a message of sucess on a pop appears"
        assert addedToCartPopUp.getsuccessMessage() == "Product successfully added to your shopping cart"

        and: "the itens are in the cart"
        OrderPage orderPage = addedToCartPopUp.proceedToCheckOut()
        def qtdInCart = orderPage.getCartQuantity()
        assert qtdInCart == itensNames.size().toString()

        and: "also the itens are order"
        def listProducts = orderPage.getProductsNames()
        itensNames.each {
            assert listProducts.contains(it)
        }
    }
}
