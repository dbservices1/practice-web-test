package pt.dbservices.web.PageObject

import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.PageFactory

class BasePage {

    protected final WebDriver driver;

    BasePage(WebDriver driver){
        this.driver = driver
        PageFactory.initElements(driver, this)
    }

    def refresh(){
        PageFactory.initElements(driver, this)
    }
}
