package pt.dbservices.web.PageObject

import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy

class HomePage extends BasePage {

    @FindBy(id = "search_query_top")
    WebElement txtSearch

    HomePage(WebDriver driver) {
        super(driver)
    }


    def searchItem(searchItem){
        txtSearch.sendKeys(searchItem)
        txtSearch.sendKeys(Keys.ENTER)
        return new SearchListPage(driver)
    }

}
