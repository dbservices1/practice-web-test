package pt.dbservices.web.PageObject


import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait

class AddedToCartPopUp extends BasePage {

    @FindBy(id = "layer_cart")
    WebElement popUpCart

    @FindBy(xpath = "//*[@id=\"layer_cart\"]/div[1]/div[1]/h2")
    WebElement successMessage

    @FindBy(xpath = "//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/a")
    WebElement btnContinueToCheckOut

    @FindBy(className = "continue")
    WebElement btnContinueShopping

    AddedToCartPopUp(WebDriver driver) {
        super(driver)
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(popUpCart))
        super.refresh()
    }

    def getsuccessMessage(){
        return successMessage.getText()
    }

    def proceedToCheckOut() {
        btnContinueToCheckOut.click()
         return new OrderPage(driver)
    }

    def continueShopping(){
        btnContinueShopping.click()
        return new SearchListPage(driver)
    }
}
