package pt.dbservices.web.PageObject

import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy

class OrderPage extends BasePage {

    @FindBy(className = "cart_item")
    List<WebElement> orderCartItens

    @FindBy(className  = "ajax_cart_quantity")
    WebElement cartQuantity

    OrderPage(WebDriver driver) {
        super(driver)
    }

    def getCartQuantity() {
        return cartQuantity.getText()
    }

    def getProductsNames() {
        def listNames = []
        orderCartItens.each { it ->
            def productName = it.findElement(By.className("product-name")).getText()
            listNames.add(productName)
        }
        return listNames
    }

}
