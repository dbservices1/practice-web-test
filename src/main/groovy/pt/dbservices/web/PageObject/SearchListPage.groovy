package pt.dbservices.web.PageObject

import org.apache.tools.ant.taskdefs.Sleep
import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.support.FindBy

class SearchListPage extends BasePage {

    @FindBy(id = "productsSortForm")
    WebElement productsSortForm

    @FindBy(className = "product-container")
    List<WebElement> listSearchedItens

    @FindBy(className  = "ajax_add_to_cart_button")
    List<WebElement> listOfAddToCart

    @FindBy(className  = "ajax_cart_quantity")
    WebElement cartQuantity

    JavascriptExecutor javascriptExecutor

    Actions actions

    SearchListPage(WebDriver driver) {
        super(driver)
        javascriptExecutor = (JavascriptExecutor) driver
        actions = new Actions(driver)
    }

    def getCartQuantity() {
        return cartQuantity.getText()
    }

    def getNameItemToAddInCart(orderNumbItem){
        def productxPath = "//*[@id=\"center_column\"]/ul/li[${orderNumbItem}]/div/div[2]/h5/a"
        WebElement tstProductName = driver.findElement(By.xpath(productxPath))
        def productNameSelected = tstProductName.getText()
        return productNameSelected
    }

    def addItemToCart(orderNumbItem) {
        def iteratorItem = orderNumbItem - 1

        javascriptExecutor.executeScript("arguments[0].scrollIntoView(true);", listSearchedItens[iteratorItem])
        actions.moveToElement(listSearchedItens[iteratorItem]).perform()

        WebElement addToCart = listOfAddToCart.find { it ->
            it.isDisplayed() == true
        }
        addToCart.click()
        return new AddedToCartPopUp(driver)
    }

}
