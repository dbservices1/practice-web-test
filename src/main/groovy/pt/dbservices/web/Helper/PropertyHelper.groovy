package pt.dbservices.web.Helper

class PropertyHelper {

    def config

    PropertyHelper(){
        Properties properties = new Properties()
        def pathName = System.getProperty("user.dir") + "/src/test/resources/config.properties"
        File propsFile = new File(pathName)
        properties.load(propsFile.newDataInputStream())
        config = new ConfigSlurper().parse(properties)
    }

    def getWebSiteUrl(String serviceName) {
        return config.web.site.get(serviceName).base.url
    }

}
