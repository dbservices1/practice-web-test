package pt.dbservices.web.SetupDriver

import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.firefox.FirefoxDriver

import java.util.concurrent.TimeUnit

class DriverSetup {

    static def getDriver() {
        WebDriver driver
        def webBrowser = System.getProperty("webBrowser").toLowerCase()
        switch (webBrowser) {
            case "chrome":
                driver = new ChromeDriver()
                break
            case "firefox":
                driver = new FirefoxDriver()
                break
            default:
                driver = new ChromeDriver()
                break
        }
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS)
        driver.manage().window().maximize()
        return driver
    }

    static def setSystemPropertyDriver(){
        def webBrowser = System.getProperty("webBrowser").toLowerCase()
        switch (webBrowser) {
            case "chrome":
                setChromeDriver()
                break
            case "firefox":
                setFireFoxDriver()
                break
            default:
                setChromeDriver()
                break
        }
    }

    static def setChromeDriver(){
        def pathChromeDriver = System.getProperty("user.dir") + "/src/main/resources/chromedriver"
        System.setProperty("webdriver.chrome.driver", pathChromeDriver)
    }

    static def setFireFoxDriver(){
        def pathGeckoDriver = System.getProperty("user.dir") + "/src/main/resources/geckodriver"
        System.setProperty("webdriver.gecko.driver", pathGeckoDriver)
    }
}
